import json
import os
from abc import ABC, abstractmethod
from typing import Optional
from uuid import UUID

from pydantic import BaseModel

from file_manager import FileManager
from logger import config_logger as logger

BASE_PATH_CONFIG = os.environ.get('BASE_PATH_CONFIG', 'tenants/')
FORM_SCHEMA_PATH = '../config'

SERVICE_PATH = BASE_PATH_CONFIG + '{tenant_id}/{service_id}.json'
SERVICE_FORM_SCHEMA_PATH = f'{FORM_SCHEMA_PATH}/service-form-schema.json'

TENANT_PATH = BASE_PATH_CONFIG + '{tenant_id}/tenant.json'
TENANT_FORM_SCHEMA_PATH = f'{FORM_SCHEMA_PATH}/tenant-form-schema.json'

DISABLE_CONFIG = {'active': False}

file_manager = FileManager()


class SchemaNotFound(Exception):  # noqa: N818
    """Used to handle when the resource is not found and return 404"""

    pass


class ConfigurationNotFound(Exception):  # noqa: N818
    """
    Used to handle when the resource is not found and return 404
    """

    pass


class ConfigurationNotAllowed(Exception):  # noqa: N818
    """
    Used to handle all known non-critical exception and return 400
    """

    pass


class ConfiguratorEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, UUID):
            return str(o)
        return o.__dict__


class Configurator(ABC):
    def json(self) -> str:
        return json.dumps(self, cls=ConfiguratorEncoder)

    def dict(self) -> dict:
        return json.loads(json.dumps(self, cls=ConfiguratorEncoder))

    @abstractmethod
    def get_configuration(self):
        pass

    @abstractmethod
    def update_or_save_configuration(self, tenant, update):
        pass

    @abstractmethod
    def update_existing_configuration(self, new_configuration):
        pass


def load_json_file(path: str) -> dict:
    """handles the loading of a json file and returns a dictionary"""
    if not file_manager.file_exists(path=path):
        logger.debug(f'Configuration not found at path: {path}')
        raise ConfigurationNotFound('Configuration not found')
    try:
        json_file = json.loads(file_manager.get_file(path=path))
    except json.JSONDecodeError as jde:
        logger.error(f'Json at path {path} not valid. Error {jde}')
        raise ConfigurationNotFound('The json in the filesystem is not valid.')
    except Exception as e:
        logger.error(f'General error at path {path}: {e}')
        raise ConfigurationNotFound('Error trying to retrieve the configuration')
    else:
        return json_file


def save_json_file(data: BaseModel, path: str, update: bool) -> dict:
    """
    saves a json file on the filesystem
    creating the path if it doesn't exist

    Parameters
    ----------
    data: BaseModel
        data is a pydantic object with json() method
    path: str
        the path to save the data as json,
        will be created if it doesn't exist
    update: bool
        if the file has to be updated or not
    """
    try:
        if not update and file_manager.file_exists(path=path) and load_json_file(path=path)['active'] is True:
            logger.debug(
                f'Path error - configuration at path {path} already exists. POST not allowed, use PUT or PATCH.'
            )
            raise ConfigurationNotAllowed(
                'The file already exists, use method PUT or PATCH to edit the configuration',
            )
        file_manager.save_file(path=path, data=data.json())
        return json.loads(data.json())
    except ConfigurationNotAllowed:
        raise
    except Exception as e:
        logger.error(f'Path {path} error: {str(e)}')
        raise ConfigurationNotFound('Error trying to save the resource in the filesystem, path error')


def update_configuration(path: str, new_configuration: dict) -> dict:
    """
    Updates the configuration of an existent
    tenant or service in the filesystem.

    Parameters
    ----------
    path: str
        the path for the json to update
    new_configuration: dict
        the object containing the fields to update
    """
    if 'id' in new_configuration or 'tenant_id' in new_configuration:
        raise ConfigurationNotAllowed('Edit id or tenant_id not allowed')
    return dict(load_json_file(path), **new_configuration)


# --------------------- TENANT ---------------------


class TenantUpdate(BaseModel):
    tax_identification_number: str
    company_name: str
    active: bool

    def to_dict(self) -> dict:
        return json.loads(json.dumps(self, cls=ConfiguratorEncoder))


class Tenant(TenantUpdate):
    id: UUID


class TenantConfigurator(Configurator):
    def __init__(self, tenant_id: UUID) -> None:
        self.path = TENANT_PATH.format(tenant_id=tenant_id)

    def get_configuration(self) -> Tenant:
        return Tenant.parse_obj(load_json_file(self.path))

    def update_or_save_configuration(self, tenant: Tenant, update: bool) -> dict:
        return save_json_file(data=tenant, path=self.path, update=update)

    def update_existing_configuration(self, new_configuration: dict) -> dict:
        data = Tenant.parse_obj(update_configuration(path=self.path, new_configuration=new_configuration))
        return save_json_file(data=data, path=self.path, update=True)

    def disable_configuration(self):
        data = Tenant.parse_obj(update_configuration(path=self.path, new_configuration=DISABLE_CONFIG))
        return save_json_file(data=data, path=self.path, update=True)


def get_tenant_schema() -> json:
    try:
        with open(TENANT_FORM_SCHEMA_PATH) as f:
            obj = json.load(f)
        return json.dumps(obj)
    except FileNotFoundError:
        raise SchemaNotFound('Form schema not available')


# --------------------- SERVICE ---------------------


class ServiceUpdate(BaseModel):
    tenant_id: UUID
    active: bool

    def to_dict(self) -> dict:
        return json.loads(json.dumps(self, cls=ConfiguratorEncoder))


class Service(ServiceUpdate):
    id: UUID


class ServiceConfigurator(Configurator):
    def __init__(self, tenant_id: Optional[UUID], service_id: UUID) -> None:
        self.path = SERVICE_PATH.format(tenant_id=tenant_id, service_id=service_id)

    def get_configuration(self) -> Service:
        return Service.parse_obj(load_json_file(self.path))

    def update_or_save_configuration(self, service: Service, update: bool) -> dict:
        return save_json_file(data=service, path=self.path, update=update)

    def update_existing_configuration(self, new_configuration: dict) -> dict:
        data = Service.parse_obj(update_configuration(path=self.path, new_configuration=new_configuration))
        return save_json_file(data=data, path=self.path, update=True)

    def disable_configuration(self):
        data = Service.parse_obj(update_configuration(path=self.path, new_configuration=DISABLE_CONFIG))
        return save_json_file(data=data, path=self.path, update=True)


def get_service_schema() -> json:
    try:
        with open(SERVICE_FORM_SCHEMA_PATH) as f:
            obj = json.load(f)
        return json.dumps(obj)
    except FileNotFoundError:
        raise SchemaNotFound('Form schema not available')
