#!/usr/bin/env python
import ast
import os
from contextlib import asynccontextmanager
from uuid import UUID

import uvicorn as uvicorn
from dotenv import load_dotenv
from fastapi import Body, Depends, FastAPI, HTTPException, Path, Response, status
from fastapi.responses import RedirectResponse
from pydantic import ValidationError

from __init__ import __version__ as app_version
from configurator import (
    ConfigurationNotAllowed,
    ConfigurationNotFound,
    SchemaNotFound,
    Service,
    ServiceConfigurator,
    ServiceUpdate,
    Tenant,
    TenantConfigurator,
    TenantUpdate,
    get_service_schema,
    get_tenant_schema,
)
from logger import proxy_logger as logger
from mykafka import Kafka
from pagopa import Pagopa, PagopaException
from payment_params import PaymentParams

load_dotenv()

logger.info('Starting')
APP_HOST = os.environ.get('APP_HOST', '0.0.0.0')
APP_PORT = os.environ.get('APP_PORT', 8000)
EXTERNAL_API_URL = os.environ.get('EXTERNAL_API_URL', 'http://0.0.0.0:8000')
INTERNAL_API_URL = os.environ.get('INTERNAL_API_URL', 'http://0.0.0.0:8000')
PAGOPA_CONFIG_PATH = os.environ.get('PAGOPA_CONFIG_PATH', '../tenants')
PAGOPA_PAYMENT_URL = os.environ.get('PAGOPA_PAYMENT_URL')
KAFKA_TOPIC_NAME = os.environ.get('KAFKA_TOPIC_NAME', 'payments')
KAFKA_BOOTSTRAP_SERVERS = os.environ.get('KAFKA_BOOTSTRAP_SERVERS', 'localhost:29092').split(',')
KAFKA_GROUP_ID = os.environ.get('KAFKA_GROUP_ID', 'pagopa_payment_proxy')
FASTAPI_BASE_URL = os.getenv('FASTAPI_BASE_URL')

kafka = Kafka(
    topic=KAFKA_TOPIC_NAME,
    bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
    group_id=KAFKA_GROUP_ID,
    enable_auto_commit=True,
    seek_to_beginning=False,
    app_id=f'pagopa-payment-proxy:{app_version}',
)


@asynccontextmanager
async def manage_startup_and_shutdown(app: FastAPI):
    logger.info('Initializing API ...')

    await kafka.initialize()
    await kafka.consume(pagopa.process_payment)

    yield

    logger.info('Stopping consumer')
    logger.info('Stopping application')

    kafka.consumer_task.cancel()
    await kafka.consumer.stop()


# instantiate the API
app = FastAPI(
    title='Pagopa Payment Proxy',
    version=app_version,
    openapi_url=f'{FASTAPI_BASE_URL or ""}/openapi.json',
    lifespan=manage_startup_and_shutdown,
)


@app.get('/status', tags=['Status'])
async def check_status():
    try:
        return {'status': 'Everything OK!'}
    except Exception:
        raise HTTPException(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)


@app.get('/online-payment/{payment_id}', tags=['Payment'])
async def get_online_url(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        payment_url, payment = pagopa.get_online_url(payment_id=payment_id)
        logger.debug(payment_url)
        await kafka.send_event(payment)
        return RedirectResponse(payment_url)
    except PagopaException as pe:
        return RedirectResponse((ast.literal_eval(str(pe)))['url_ko'])


@app.get('/landing/{payment_id}', tags=['Payment'])
async def get_landing_url(
    payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74'),  # noqa: B008
    params: PaymentParams = Depends(),  # noqa: B008
):
    try:
        redirect_url, payment = pagopa.get_landing_url(payment_id=payment_id, payment_status=params.status)
        await kafka.send_event(payment)
        return RedirectResponse(redirect_url)
    except PagopaException:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item not found')


@app.get('/tenants/schema', tags=['Tenants'])
async def get_tenant_form_schema():
    try:
        return Response(get_tenant_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))


@app.get('/tenants/{tenant_id}', response_model=Tenant, tags=['Tenants'])
async def get_tenant_configuration(
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        return tc.get_configuration()
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ValidationError as e:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e))


@app.post('/tenants', status_code=status.HTTP_201_CREATED, tags=['Tenants'])
async def save_tenant_configuration(
    configuration: Tenant = Body(  # noqa: B008
        example={
            'id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6',
            'tax_identification_number': '77777777777',
            'company_name': 'Comune di Bugliano',
            'active': True,
        },
    ),
):
    try:
        tc = TenantConfigurator(tenant_id=configuration.id)
        tc.update_or_save_configuration(tenant=configuration, update=False)
        pagopa.add_or_update_tenant_conf(configuration.to_dict())
        logger.debug(f'Updated tenant conf dict: {pagopa.tenants}')
        return Response(status_code=status.HTTP_201_CREATED, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.put('/tenants/{tenant_id}', tags=['Tenants'])
async def update_tenant_configuration(
    configuration: TenantUpdate,
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        config_data = Tenant(**configuration.to_dict(), id=tenant_id)
        tc.update_or_save_configuration(tenant=config_data, update=True)
        pagopa.add_or_update_tenant_conf(config_data.to_dict())
        logger.debug(f'Updated tenant conf dict: {pagopa.tenants}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.patch('/tenants/{tenant_id}', tags=['Tenants'])
async def update_existing_tenant_configuration(
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
    new_configuration: dict = Body(example={'id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6'}),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        config_data = tc.update_existing_configuration(new_configuration=new_configuration)
        pagopa.add_or_update_tenant_conf(config_data)
        logger.debug(f'Updated tenant conf dict: {pagopa.tenants}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.delete('/tenants/{tenant_id}', status_code=status.HTTP_204_NO_CONTENT, tags=['Tenants'])
async def delete_tenant(tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6'))):  # noqa: B008
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        tc.disable_configuration()
        pagopa.delete_tenant_conf(tenant_id)
        logger.debug(f'Updated tenant conf dict: {pagopa.tenants}')
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    except (ConfigurationNotFound, KeyError) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


# API service configuration


@app.get('/services/schema', tags=['Services'])
async def get_service_form_schema():
    try:
        return Response(get_service_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))


@app.get('/services/{service_id}', response_model=Service, tags=['Services'])
async def get_service_configuration(
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
):
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(pagopa.services[str(service_id)]['tenant_id'])
            if str(service_id) in pagopa.services
            else None,
            service_id=service_id,
        )
        return sc.get_configuration()
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ValidationError as e:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(e))


@app.post('/services', status_code=status.HTTP_201_CREATED, tags=['Services'])
async def save_service_configuration(
    configuration: Service = Body(  # noqa: B008
        example={
            'tenant_id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6',
            'id': '23d57b65-5eb9-4f0a-a507-fbcf3057b248',
            'active': True,
        },
    ),
):
    try:
        if not str(configuration.tenant_id) in pagopa.tenants:
            raise ConfigurationNotFound(f"Tenant {configuration.tenant_id} doesn't exist")
        sc = ServiceConfigurator(tenant_id=configuration.tenant_id, service_id=configuration.id)
        sc.update_or_save_configuration(service=configuration, update=False)
        pagopa.add_or_update_service_conf(configuration.to_dict())
        logger.debug(f'Updated services conf dict: {pagopa.services}')
        return Response(status_code=status.HTTP_201_CREATED, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.put('/services/{service_id}', tags=['Services'])
async def update_service_configuration(
    configuration: ServiceUpdate,
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
):
    try:
        if not str(configuration.tenant_id) in pagopa.tenants:
            raise ConfigurationNotFound(f"Tenant {configuration.tenant_id} doesn't exist")
        sc = ServiceConfigurator(tenant_id=configuration.tenant_id, service_id=service_id)
        config_data = Service(**configuration.to_dict(), id=service_id)
        sc.update_or_save_configuration(service=config_data, update=True)
        pagopa.add_or_update_service_conf(config_data.to_dict())
        logger.debug(f'Updated services conf dict: {pagopa.services}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.patch('/services/{service_id}', tags=['Services'])
async def update_existing_service_configuration(
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
    new_configuration: dict = Body(example={'active': False}),  # noqa: B008
):
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(pagopa.services[str(service_id)]['tenant_id'])
            if str(service_id) in pagopa.services
            else None,
            service_id=service_id,
        )
        config_data = sc.update_existing_configuration(new_configuration=new_configuration)
        pagopa.add_or_update_service_conf(config_data)
        logger.debug(f'Updated services conf dict: {pagopa.services}')
        return Response(status_code=status.HTTP_200_OK, media_type='application/json')
    except ConfigurationNotFound as ce:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(ce))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@app.delete('/services/{service_id}', status_code=status.HTTP_204_NO_CONTENT, tags=['Services'])
async def delete_service(service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248')):  # noqa: B008
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(pagopa.services[str(service_id)]['tenant_id'])
            if str(service_id) in pagopa.services
            else None,
            service_id=service_id,
        )
        sc.disable_configuration()
        pagopa.delete_service_conf(service_id)
        logger.debug(f'Updated service conf dict: {pagopa.services}')
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    except (ConfigurationNotFound, KeyError) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    except ConfigurationNotAllowed as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


pagopa = Pagopa(
    config_path=PAGOPA_CONFIG_PATH,
    payment_url=PAGOPA_PAYMENT_URL,
    app_online_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_online_url', payment_id='{id}')}",
    app_landing_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_landing_url', payment_id='{id}')}",
)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=APP_HOST,
        port=APP_PORT,
        proxy_headers=True,
        forwarded_allow_ips='*',
        access_log=False,
    )
