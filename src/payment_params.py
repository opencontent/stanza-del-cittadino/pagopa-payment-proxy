from fastapi import Query


class PaymentParams:
    def __init__(self, status: str = Query(default=None, description='Stato del pagamento')):  # noqa: B008
        self.status = status
