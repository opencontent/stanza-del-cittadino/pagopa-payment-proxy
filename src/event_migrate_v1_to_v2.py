import json
import os
from typing import List, Optional, Union

from dotenv import load_dotenv
from file_manager import FileManager

# Import version 2 model
from oc_python_sdk.models.payment import Payment, PaymentDataSplit

load_dotenv()

STORAGE_KEY = os.environ.get('STORAGE_KEY', '../')
STORAGE_BUCKET_NAME = os.environ.get('STORAGE_BUCKET_NAME', 'payments')
BASE_PATH_EVENT = os.environ.get('BASE_PATH_EVENT', 'payments/')


def update_split_to_v2(split_data: Optional[Union[dict, list]]) -> List[Optional[PaymentDataSplit]]:
    if split_data is None:
        # If the split field is null, set it to an empty array
        return []
    elif isinstance(split_data, dict):
        # If the split field is a dictionary
        # Check if there are values in the dictionary
        if not split_data:
            # If the dictionary is empty, set it to an empty array
            return []
        else:
            # Otherwise, create a PaymentDataSplit object for each entry in the dictionary
            return [PaymentDataSplit(code=key, amount=value, meta={}) for key, value in split_data.items()]
    elif isinstance(split_data, list):
        # If the split field is already an array, leave it as it is
        if not split_data or split_data[0].get('code'):
            return split_data
        return [
            PaymentDataSplit(
                code=split_item['split_id'],
                amount=split_item['split_amount'],
                meta={key: value for key, value in split_item.items() if key not in ['split_id', 'split_amount']},
            )
            for split_item in split_data
        ]
    else:
        # If the split field is of an unsupported type, raise an exception
        raise ValueError("The 'split' field is not of a supported type")


def update_json_to_v2(json_data):
    # Load JSON as dictionary
    payment_v1_data = json.loads(json_data)

    # Check the event version
    event_version = payment_v1_data.get('event_version', None)
    if float(event_version) != 1:
        # If the event version is not "1.0", do not perform any processing
        return json.dumps(payment_v1_data)

    # Update the event_version field to "2.0"
    payment_v1_data['event_version'] = '2.0'

    # Update the split field
    payment_v1_data['payment']['split'] = update_split_to_v2(payment_v1_data['payment'].get('split', None))

    # Create a Payment object using the values from the version 1 dictionary
    payment_v2 = Payment(**payment_v1_data)

    # Serialize the Payment object as JSON
    return payment_v2.json()


def main():
    # Path to the folder containing JSON files
    folder_path = f'{STORAGE_KEY}{STORAGE_BUCKET_NAME}/{BASE_PATH_EVENT}'

    updated_files_count = 0

    # Iterate through all files in the folder
    file_manager = FileManager()
    for key, json_data in file_manager.get_all_files(BASE_PATH_EVENT).items():
        # Update the JSON to version 2 only if the event is in version 1.0
        updated_json = update_json_to_v2(json.dumps(json_data))

        # Overwrite the file with the updated JSON only if necessary
        if updated_json != json.dumps(json_data):
            filename = key.split('_')[0]
            file_manager.save_file(path=os.path.join(folder_path, filename), data=updated_json)

            updated_files_count += 1
            print(f'File {filename} updated successfully.')

    print(f'{updated_files_count} files have been updated.' if updated_files_count else 'No files have been updated.')


if __name__ == '__main__':
    main()
