import json
import os
from datetime import datetime
from enum import Enum

import requests
from oc_python_sdk.models.payment import Payment, PaymentStatus
from starlette import status

from file_manager import FileManager
from logger import pagopa_logger as logger

BASE_PATH_CONFIG = os.environ.get('BASE_PATH_CONFIG', 'tenants/')
BASE_PATH_EVENT = os.environ.get('BASE_PATH_EVENT', 'payments/')
CANCEL_PAYMENT = True if os.environ.get('CANCEL_PAYMENT', 'false').lower() == 'true' else False


class PagopaException(Exception):  # noqa: N818
    pass


class LinkType(Enum):
    INTERNAL_LINK = 'internal'
    EXTERNAL_LINK = 'external'


class Pagopa:
    def __init__(
        self,
        payment_url,
        app_online_url,
        app_landing_url,
        config_path,
    ):
        logger.info('Starting PAGOPA initialization')

        self.file_manager = FileManager()
        self.config_path = config_path

        self.tenants = {}
        self.services = {}

        self.payment_url = payment_url

        self.app_online_url = app_online_url
        self.app_landing_url = app_landing_url

        self._load_tenants_and_services()

        logger.info('PAGOPA initialization completed')

    def _load_tenants_and_services(self):
        for key, value in self.file_manager.get_all_files(BASE_PATH_CONFIG).items():
            if 'active' not in value or value['active']:
                if 'tenant' in key:
                    self.add_or_update_tenant_conf(value)
                else:
                    self.add_or_update_service_conf(value)
        logger.debug(self.tenants)
        logger.debug(self.services)

    async def process_payment(self, event, **kwargs):
        payment = Payment(**event)
        if self._is_external(payment):
            logger.info(f"Event {event['id']} from external source: Processing event")
            payment.links.offline_payment.url = None
            payment.links.offline_payment.method = None
            payment.links.receipt.url = None
            payment.links.receipt.method = None
            self.file_manager.save_file(path=f'{BASE_PATH_EVENT}{str(payment.id)}.json', data=json.dumps(event))
            return json.loads(payment.json())
        else:
            logger.info(f"Event {event['id']} from application {event['remote_id']}: Processing not required")
            return None

    def _is_external(self, payment):
        """
        :param payment:
        :return: bool
        Checks if a payment was processed by the proxy
        or if it was imported from an external source
        """
        try:
            if payment.status == PaymentStatus.STATUS_PAYMENT_PENDING and not self._payment_exists(str(payment.id)):
                self._get_service_by_identifier(service_id=str(payment.service_id))
                return True
        except PagopaException:
            return False
        return False

    def get_online_url(self, payment_id: str, url_type: LinkType = LinkType.EXTERNAL_LINK):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_online_url.replace('{id}', payment_id)

        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)
        payload = self._get_creation_request(payment)

        logger.debug(
            f'Event {payment.id} from application {payment.remote_id}: Sending creation request with data {payload}',
        )
        response = requests.post(url=self.payment_url, json=payload, allow_redirects=False)
        if response.status_code != status.HTTP_302_FOUND:
            error = response.json()
            logger.error(f'An error occurred: {error["title"]}: {error["detail"]}')
            error = {
                'error': f'An error occurred: {error["title"]}: {error["detail"]}',
                'url_ko': f'{payment.links.online_payment_landing.url}?esito=KO'
            }
            raise PagopaException(str(error))

        payment = payment.update_time('links.online_payment_begin.last_opened_at')
        payment = payment.update_time('updated_at')
        return response.headers['Location'], json.loads(payment.json())

    def get_landing_url(
        self,
        payment_id: str,
        payment_status: str = None,
        url_type: LinkType = LinkType.EXTERNAL_LINK,
    ):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_landing_url.replace('{id}', payment_id)

        payment, _ = self._get_payment_and_tenant(payment_id=payment_id)
        payment = payment.update_time('links.online_payment_landing.last_opened_at')
        payment = payment.update_time('updated_at')

        if payment_status == 'OK':
            logger.info(f'Payment {payment.payment.iuv} from external source: Callback with status {payment_status}')
            payment.status = PaymentStatus.STATUS_COMPLETE
            payment.payment.paid_at = datetime.now().replace(microsecond=0).astimezone().isoformat()
        else:
            logger.warning(
                f'Payment {payment.payment.iuv} from external source: Callback with status {payment_status}',
            )

        return payment.links.online_payment_landing.url, json.loads(payment.json())

    def _get_creation_request(self, payment: Payment):
        logger.debug(f'Event {payment.id} from application {payment.remote_id}: Prepare payload for creation request')

        _, tenant = self._get_payment_and_tenant(payment_id=str(payment.id))

        landing_url = self.get_landing_url(payment_id=str(payment.id), url_type=LinkType.INTERNAL_LINK)

        payload = {
            'emailNotice': payment.payer.email,
            'paymentNotices': [
                {
                    'noticeNumber': payment.payment.notice_code,
                    'fiscalCode': tenant['tax_identification_number'],
                    'amount': payment.payment.amount * 100,
                    'companyName': tenant['company_name'],
                    'description': payment.reason,
                },
            ],
            'returnUrls': {
                'returnOkUrl': f'{landing_url}?status=OK',
                'returnCancelUrl': f'{landing_url}?status=KO',
                'returnErrorUrl': f'{landing_url}?status=KO',
            },
        }

        return payload

    def _get_tenant_by_identifier(self, tenant_id: str):
        if tenant_id in self.tenants:
            return self.tenants[tenant_id]
        logger.error(f'Missing configuration for tenant identifier {tenant_id}')
        raise PagopaException('Tenant configuration missing')

    def _get_service_by_identifier(self, service_id: str):
        if service_id in self.services:
            return self.services[service_id]
        logger.error(f'Missing configuration for service identifier {service_id}')
        raise PagopaException('Service configuration missing')

    def _get_payment_and_tenant(self, payment_id: str, cancel_payment=False):
        payment = self.file_manager.get_file(path=f'{BASE_PATH_EVENT}{payment_id}.json')
        if not payment:
            raise PagopaException(f'Missing payment {payment_id}')
        payment = Payment(**json.loads(payment))
        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        return payment, tenant

    def add_or_update_service_conf(self, config):
        self.services[config['id']] = config

    def add_or_update_tenant_conf(self, config):
        self.tenants[config['id']] = config

    def delete_tenant_conf(self, tenant_id):
        del self.tenants[str(tenant_id)]

    def delete_service_conf(self, service_id):
        del self.services[str(service_id)]

    def _payment_exists(self, payment_id):
        return self.file_manager.file_exists(path=f'{BASE_PATH_EVENT}{payment_id}.json')

    @staticmethod
    def _cancel_payment(payment_id):
        logger.warning(f'Canceling payment {payment_id}')
        return {
            'id': payment_id,
            'status': PaymentStatus.STATUS_PAYMENT_FAILED.value,
        }
